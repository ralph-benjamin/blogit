<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder {

	public function run()
	{
		$commentA = new Comment;
		$commentA->body = 'Great stuff.';
		$commentA->save();

		$commentB = new Comment;
		$commentB->body = 'I Love it.';
		$commentB->save();

		$commentC = new Comment;
		$commentC->body = 'Thanks for the post.';
		$commentC->save();

		$commentD = new Comment;
		$commentD->body = 'Pretty cool.';
		$commentD->save();

		$commentE = new Comment;
		$commentE->body = 'Wow, interesting!!';
		$commentE->save();

		$post1 = Post::find(1);
		$post2 = Post::find(2);


		$commentA = $post1->comments()->save($commentA);
		$commentB = $post1->comments()->save($commentB);
		$commentC = $post1->comments()->save($commentC);
		$commentD = $post2->comments()->save($commentD);
		$commentE = $post2->comments()->save($commentE);


		##### Sub comments

		$sub_commentA1 = new Comment;
		$sub_commentA1->body = 'I totally agree.';
		$sub_commentA1->save();

		$sub_commentA2 = new Comment;
		$sub_commentA2->body = 'I second that.';
		$sub_commentA2->save();

		$sub_commentA3 = new Comment;
		$sub_commentA3->body = 'Yup.';
		$sub_commentA3->save();

		$sub_commentB1 = new Comment;
		$sub_commentB1->body = 'Exactly, good point.';
		$sub_commentB1->save();


		$sub_commentB2 = new Comment;
		$sub_commentB2->body = 'Yes, I agree!';
		$sub_commentB2->save();

		$sub_commentC = new Comment;
		$sub_commentC->body = 'Thanks for the post.';
		$sub_commentC->save();

		$sub_commentA1 = $commentA->replies()->save($sub_commentA1);
		$sub_commentA2 = $commentA->replies()->save($sub_commentA2);
		$sub_commentA3 = $commentA->replies()->save($sub_commentA3);

		$sub_commentB1 = $commentB->replies()->save($sub_commentB1);
		$sub_commentB2 = $commentB->replies()->save($sub_commentB2);


	}

}