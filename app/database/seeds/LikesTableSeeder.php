<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class LikesTableSeeder extends Seeder {

	public function run()
	{
		$comments = Comment::all();

		foreach ($comments as $c) {

			$random = rand(20, 50);
			for ($i=0; $i < $random; $i++) { 
				$l = new Like;
				$l->like = true;
				$l->save();

				$l = $c->likes()->save($l);
			}
			
			
		}
		

		$posts = Post::all();

		foreach ($posts as $p) {

			$random = rand(20, 50);
			for ($i=0; $i < $random; $i++) { 
				$l = new Like;
				$l->like = true;
				$l->save();

				$l = $p->likes()->save($l);
			}
			
			
		}

	}

}