<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder {

	public function run()
	{
		$post1 = new Post;
		$post1->title = 'My first awesome post.';
		$post1->body = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam vero, reiciendis maxime labore repellat distinctio esse iste ex rerum sint? Quasi iusto officiis, incidunt?';
		$post1->save();

		$post2 = new Post;
		$post2->title = 'My second awesome post.';
		$post2->body = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi excepturi a velit voluptas architecto quis.';
		$post2->save();

		$post3 = new Post;
		$post3->title = 'My third awesome post.';
		$post3->body = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias ea officia eos vitae dolorum ullam, architecto laboriosam! Accusantium voluptas, odit.';
		$post3->save();

		$u1 = User::find(1);
		$u2 = User::find(2);

		$post1 = $u1->posts()->save($post1);
		$post2 = $u2->posts()->save($post2);
		$post3 = $u1->posts()->save($post3);
	}

}