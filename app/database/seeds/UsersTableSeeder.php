<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$a = new User;
		$a->first_name = 'John';
		$a->last_name = 'Smith';
		$a->email = 'johnsmith@test.tld';
		$a->password = Hash::make('secret');
		$a->save();

		$a = new User;
		$a->first_name = 'Michael';
		$a->last_name = 'Jackson';
		$a->email = 'mike@test.tld';
		$a->password = Hash::make('secret');
		$a->save();

	}

}