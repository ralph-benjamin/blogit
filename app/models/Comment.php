<?php

class Comment extends \Eloquent {
	protected $fillable = ['body'];

	public function commentable()
    {
        return $this->morphTo();
    }

    public function replies()
	{
		return $this->hasMany('Comment', 'comment_parent');
	}

	public function likes()
    {
        return $this->morphMany('Like', 'likeable');
    }

}