<?php

class Like extends \Eloquent {
	protected $fillable = ['liked'];

	public function likeable()
    {
        return $this->morphTo();
    }
}