<?php

class Post extends \Eloquent {
	protected $fillable = ['title', 'body'];

	public function user()
    {
        return $this->belongsTo('User');
    }

    public function comments()
    {
        return $this->morphMany('Comment', 'commentable')->where('comment_parent', 0);
    }

    public function likes()
    {
        return $this->morphMany('Like', 'likeable');
    }
}