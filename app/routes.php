<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{

	$posts = Post::all();

	return View::make('comments')->with('posts', $posts);

});

Route::get('/users', function()
{

	return User::all();

});

Route::get('/posts', function()
{

	return Post::with('comments')->get();

});

