<!DOCTYPE html>
<html lang="en">
<head>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	<style>
		* {
			line-height: 25px;
		}
		small{
			color: dimgray;
			padding: 0 10px 0 10px;
		}

		.like {
			color: rgb(160, 180, 255);
		}
	</style>
	<meta charset="UTF-8">
	<title>Display User Posts with replies/comments.</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<nav class="navbar navbar-inverse">
			  <div class="container-fluid">
			    <div class="navbar-header">
			      <a href="" class="navbar-brand">BlogIT</a>
			      <p class="navbar-text">Simple posting and commenting app. - All the data below is loaded and related dynamically, but user interactions haven't been implemented yet.</p>
			      
			    </div>
			  </div>
			</nav>
			<div class="alert alert-success" role="alert">
				Also checkout {{ link_to('users', 'users') }}, and {{ link_to('posts', 'posts') }} JSON objects (the posts route retrives associated comments too).
			</div>
		</div>
	</div>


	<div class="container">
		<div class="col-sm-8 col-sm-offset-2">
			@foreach($posts as $post)
				<div class="row">
					<div class="col-sm-8">
						<h3>{{ $post->title }}  
							<small class="like">
								{{ ($post->likes()->count()) ? '<i class="fa fa-thumbs-up"></i> ' . $post->likes()->count() : 'No likes, be the first.' }}
							</small>
						</h3>
					</div>
					<div class="col-sm-4 text-right">
						<h4> -- Written by {{ $post->user->first_name  }}</h4>
					</div>
				</div>
				<p>{{ $post->body }}</p>

				@if( $post->comments()->count() )
					<div class="well">
						<h4>Comments: </h4>
						<ul>
							@foreach($post->comments as $comment)
							<li>
								{{ $comment->body }}  <small class="like"><i class="fa fa-thumbs-up"></i> {{ $comment->likes()->count() }}</small>

								@if($comment->has('replies'))
									<ul>
									@foreach($comment->replies as $reply)
										<li>
											{{ $reply->body }}  <small class="like"><i class="fa fa-thumbs-up"></i> {{ $reply->likes()->count() }}</small>
										</li>
									@endforeach
									</ul>
								@endif

							</li>
							@endforeach
						</ul>
					</div>
				@else
					<div class="well"><h5>This post has no comments.</h5></div>
				@endif
				<hr>
			@endforeach
			
		</div>
	</div>
</body>
</html>